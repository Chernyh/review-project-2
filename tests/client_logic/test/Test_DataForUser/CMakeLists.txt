cmake_minimum_required(VERSION 3.10.2)
project(test_DataStorage)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}-pthread")

if(CMAKE_CXX_COMPILER_ID MATCHES GNU)
    set(CMAKE_CXX_FLAGS         "-Wall -Wno-unknown-pragmas -Wno-sign-compare -Woverloaded-virtual -Wwrite-strings -Wno-unused  -Wpedantic -Werror" )
    set(CMAKE_CXX_FLAGS_DEBUG   "-O0 -g3")
    set(CMAKE_CXX_FLAGS_RELEASE "-O3")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fprofile-arcs -ftest-coverage")
endif()






######################## gtest part ###############################


set(GOOGLETEST_ROOT googletest/googletest CACHE STRING "Google Test source root")

include_directories(
        ${PROJECT_SOURCE_DIR}/../../external/${GOOGLETEST_ROOT}
        ${PROJECT_SOURCE_DIR}/../../external/${GOOGLETEST_ROOT}/include
)

set(GOOGLETEST_SOURCES
        ${PROJECT_SOURCE_DIR}/../../external/${GOOGLETEST_ROOT}/src/gtest-all.cc
        ${PROJECT_SOURCE_DIR}/../../external/${GOOGLETEST_ROOT}/src/gtest_main.cc
        )

foreach(_source ${GOOGLETEST_SOURCES})
    set_source_files_properties(${_source} PROPERTIES GENERATED 1)
endforeach()

add_library(googletest_ ${GOOGLETEST_SOURCES})

add_executable(test_DataForUser main.cpp)

add_dependencies(test_DataForUser googletest_)

target_link_libraries(
        test_DataForUser
        googletest_
        pthread
        sub::dataStorage_test
)


include(CTest)
enable_testing()

add_test(unit main.cpp)

#####################################################################
