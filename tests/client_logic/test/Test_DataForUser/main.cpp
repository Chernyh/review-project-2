//
// Created by znujko on 23.10.2019.
//



#include "gtest/gtest.h"
#include "gameDataStorage_test.h"


using namespace std;





TEST(TestActionQueue, Test_Pop_Add) {
    TestActionQueue test1;

    test1.addAction(Commands::UPBUTTON);

    EXPECT_EQ(test1.actionQueue.front(), Commands::UPBUTTON);

    EXPECT_EQ(test1.popAction(), Commands::UPBUTTON);

    EXPECT_EQ(test1.popAction(), Commands::ZERO);

}




