//
// Created by znujko on 23.10.2019.
//



#include "gtest/gtest.h"
#include <utility>
#include "userDataHandler_test.h"


using namespace std;

TEST(InputHandler,Handle_add)
{
    InputHandler inputHandler(1);

    inputHandler.add(Commands::RIGHTBUTTON,20.10);

    EXPECT_EQ(inputHandler.currentTime,20.10);
    EXPECT_EQ(inputHandler.currentCommand,Commands::RIGHTBUTTON);

    CLIENT_PACKAGE packageTest = *(dynamic_cast<CLIENT_PACKAGE*>(inputHandler.handleInput()));

    EXPECT_EQ(packageTest.messageTime , 20.10);
    EXPECT_EQ(packageTest.typeOfMessage,Commands::RIGHTBUTTON);

}

TEST(UserActivityQueue,AllFunc)
{
    std::pair<double, double> pair = make_pair(1, 2);
    Position position(1, 2);
    Sprite sprite(1, 2, 3, 4);
    int playerId = 0;
    int playerHp = 100;
    Player player1(position, pair, sprite, playerId, playerHp, nullptr);

    UserActivityQueue *userActivityQueue = new UserActivityQueue(player1);



}