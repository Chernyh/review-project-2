//
// Created by znujko on 23.10.2019.
//



#include "gtest/gtest.h"
#include <utility>
#include "serverDataHandler_test.h"


using namespace std;


TEST(CLIENT_PACKAGE_TEST_,testing_methods_getType_and_constructor)
{
    CLIENT_PACKAGE packageTest(Commands::LEFTBUTTON,20.23);
    EXPECT_EQ(packageTest.typeOfMessage,Commands::LEFTBUTTON);
    EXPECT_EQ(packageTest.messageTime,20.23);



    CLIENT_PACKAGE packageTest1(Commands ::RIGHTERASE,2.23);
    EXPECT_EQ(packageTest1.typeOfMessage,Commands ::RIGHTERASE);
    EXPECT_EQ(packageTest1.messageTime,2.23);



    CLIENT_PACKAGE packageTest2(Commands ::RIGHTBUTTON,16.59);
    EXPECT_EQ(packageTest2.typeOfMessage,Commands::RIGHTBUTTON);
    EXPECT_EQ(packageTest2.messageTime,16.59);

}


TEST(TestAnswerFactory_,asd)
{

    std::pair<double, double> pair = make_pair(1, 2);
    Position position(1, 2);
    Sprite sprite(1, 2, 3, 4);


    int playerId = 0;
    int playerHp = 100;



    Player player1(position, pair, sprite, playerId, playerHp, nullptr);


    UserActivityQueue *userActivityQueue = new UserActivityQueue(player1);
    DrawHandler *drawHandler;
    SpriteGraphic *spriteGraphic = new SpriteGraphic();
    BasicPhysic *defPhysic = new BasicPhysic();
    DrawActivity *basicDraw= new DrawActivity(defPhysic,spriteGraphic);
    TestRepository testRepository(2,drawHandler,spriteGraphic);
    RepositoryManager repositoryManager(&testRepository);
    TestAnswerFactory testAnswerFactory(&repositoryManager,userActivityQueue);

}