//
// Created by znujko on 23.10.2019.
//



#include "gtest/gtest.h"
#include "dataHandler_test.h"
#include <utility>


using namespace std;


TEST(TestRepository, Update_Delete_Add_Get) {

    Player playerType;
    Bullet bulletType;
    Weapon weaponType;

    std::pair<double, double> pair = make_pair(1, 2);
    Position position(1, 2);
    Sprite sprite(1, 2, 3, 4);
    bool isPicked = true;
    int bulletCapacity = 20;
    double fireSpeed = 30;
    double timeForShoot = 0.1;

    int playerId = 0;
    int playerHp = 100;

    std::pair<double, double> pair_bullet = make_pair(1, 2);
    Position position_bullet(1, 2);
    Sprite sprite_bullet(1, 2, 3, 4);
    int damage = 20;

    Weapon weapon1(position, pair, sprite, isPicked, bulletCapacity, fireSpeed, timeForShoot);
    Player player1(position, pair, sprite, playerId, playerHp, &weapon1);


    Bullet bullet1(position_bullet, pair_bullet, sprite_bullet, damage);

    SpriteGraphic *spriteGraphic = new SpriteGraphic();
    DrawHandler *drawHandler = nullptr;
    TestRepository testRepository(4, std::make_shared<SpriteGraphic>(spriteGraphic));
    EXPECT_EQ(testRepository.map, nullptr);
    EXPECT_EQ(testRepository.mainCharacterId, 4);


    Map map = Map(2);
    testRepository.add(0, &map , UsingTypes::MAP);
    testRepository.add(0, &player1,UsingTypes::PLAYER);

    testRepository.add(0, &bullet1,UsingTypes::BULLET);
    testRepository.add(0, &weapon1,UsingTypes::WEAPON);

    EXPECT_EQ(*dynamic_cast<Player *>(testRepository.playerMap->operator[](0)), player1);
    EXPECT_EQ(*dynamic_cast<Bullet *>(testRepository.bulletMap->operator[](0)), bullet1);
    EXPECT_EQ(*dynamic_cast<Weapon *>(testRepository.weaponMap->operator[](0)), weapon1);

    EXPECT_EQ(*testRepository.map, map);

    EXPECT_EQ(*dynamic_cast<Player *>(testRepository.get(0, UsingTypes::PLAYER)), player1);
    EXPECT_EQ(*dynamic_cast<Bullet *>(testRepository.get(0, UsingTypes::BULLET)), bullet1);
    EXPECT_EQ(*dynamic_cast<Weapon *>(testRepository.get(0, UsingTypes::WEAPON)), weapon1);

    std::pair<double, double> pair_up = make_pair(1, 3);
    int damage_up = 20;

    Weapon weapon2(position, pair_up, sprite, isPicked, bulletCapacity, fireSpeed, timeForShoot);
    Player player2(position, pair_up, sprite, playerId, playerHp, &weapon1);
    Bullet bullet2(position_bullet, pair_bullet, sprite_bullet, damage_up);

    testRepository.update(0, &player2, UsingTypes::PLAYER);
    testRepository.update(0, &weapon2, UsingTypes::BULLET);
    testRepository.update(0, &bullet2, UsingTypes::WEAPON);

    EXPECT_EQ(*dynamic_cast<Player *>(testRepository.playerMap->operator[](0)), player2);
    EXPECT_EQ(*dynamic_cast<Bullet *>(testRepository.bulletMap->operator[](0)), bullet2);
    EXPECT_EQ(*dynamic_cast<Weapon *>(testRepository.weaponMap->operator[](0)), weapon2);

    testRepository.deleteData(0, UsingTypes::PLAYER);
    testRepository.deleteData(0, UsingTypes::WEAPON);
    testRepository.deleteData(0, UsingTypes::BULLET);

    EXPECT_EQ(testRepository.playerMap->empty(), true);
    EXPECT_EQ(testRepository.bulletMap->empty(), true);
    EXPECT_EQ(testRepository.weaponMap->empty(), true);


}

/*
TEST(RepositoryManager, Update_Delete_Add_Get) {
    Player playerType;
    Bullet bulletType;
    Weapon weaponType;

    std::pair<double, double> pair = make_pair(1, 2);
    Position position(1, 2);
    Sprite sprite(1, 2, 3, 4);
    bool isPicked = true;
    int bulletCapacity = 20;
    double fireSpeed = 30;
    double timeForShoot = 0.1;

    int playerId = 0;
    int playerHp = 100;

    std::pair<double, double> pair_bullet = make_pair(1, 2);
    Position position_bullet(1, 2);
    Sprite sprite_bullet(1, 2, 3, 4);
    int damage = 20;

    Weapon weapon1(position, pair, sprite, isPicked, bulletCapacity, fireSpeed, timeForShoot);
    Player player1(position, pair, sprite, playerId, playerHp, &weapon1);


    Bullet bullet1(position_bullet, pair_bullet, sprite_bullet, damage);


    SpriteGraphic *spriteGraphic = new SpriteGraphic();
    DrawHandler *drawHandler = nullptr;
    TestRepository testRepository(4, drawHandler, spriteGraphic);
    RepositoryManager repositoryManager(&testRepository);


    Map map(2);
    repositoryManager.add(0, &map);
    repositoryManager.add(0, &player1);
    repositoryManager.add(0, &bullet1);
    repositoryManager.add(0, &weapon1);
    EXPECT_EQ(*dynamic_cast<Player *>(testRepository.playerMap[0]), player1);
    EXPECT_EQ(*dynamic_cast<Bullet *>(testRepository.bulletMap[0]), bullet1);
    EXPECT_EQ(*dynamic_cast<Weapon *>(testRepository.weaponMap[0]), weapon1);
    EXPECT_EQ(*testRepository.map, map);

    EXPECT_EQ(*dynamic_cast<Player *>(repositoryManager.get(0, &playerType)), player1);
    EXPECT_EQ(*dynamic_cast<Bullet *>(repositoryManager.get(0, &bulletType)), bullet1);
    EXPECT_EQ(*dynamic_cast<Weapon *>(repositoryManager.get(0, &weaponType)), weapon1);

    std::pair<double, double> pair_up = make_pair(1, 3);
    int damage_up = 20;

    Weapon weapon2(position, pair_up, sprite, isPicked, bulletCapacity, fireSpeed, timeForShoot);
    Player player2(position, pair_up, sprite, playerId, playerHp, &weapon1);
    Bullet bullet2(position_bullet, pair_bullet, sprite_bullet, damage_up);

    repositoryManager.update(0, &player2);
    repositoryManager.update(0, &bullet2);
    repositoryManager.update(0, &weapon2);

    EXPECT_EQ(*dynamic_cast<Player *>(testRepository.playerMap[0]), player2);
    EXPECT_EQ(*dynamic_cast<Bullet *>(testRepository.bulletMap[0]), bullet2);
    EXPECT_EQ(*dynamic_cast<Weapon *>(testRepository.weaponMap[0]), weapon2);

    repositoryManager.deleteData(0, &playerType);
    repositoryManager.deleteData(0, &weaponType);
    repositoryManager.deleteData(0, &bulletType);

    EXPECT_EQ(testRepository.playerMap.empty(), true);
    EXPECT_EQ(testRepository.bulletMap.empty(), true);
    EXPECT_EQ(testRepository.weaponMap.empty(), true);


}
*/

