//
// Created by znujko on 23.10.2019.
//



#include "gtest/gtest.h"

#include <utility>
#include "data.h"


using namespace std;


TEST(testingSpriteConstructions, DefalutCopyInitialize) {
    int spriteID = 1;
    int spriteFrame = 2;
    int spriteAction = 3;
    int maxFrame = 10;

    Sprite sprite1(spriteID, spriteFrame, spriteAction, maxFrame);

    EXPECT_EQ(sprite1.spriteFrame, spriteFrame);
    EXPECT_EQ(sprite1.spriteID, spriteID);
    EXPECT_EQ(sprite1.spriteAction, spriteAction);
    EXPECT_EQ(sprite1.maxFrame, maxFrame);

    Sprite sprite2(sprite1);

    EXPECT_EQ(sprite2.spriteFrame, spriteFrame);
    EXPECT_EQ(sprite2.spriteID, spriteID);
    EXPECT_EQ(sprite2.spriteAction, spriteAction);
    EXPECT_EQ(sprite2.maxFrame, maxFrame);

    Sprite sprite3;
    EXPECT_EQ(sprite3.spriteAction, 0);
    EXPECT_EQ(sprite3.spriteID, 0);
    EXPECT_EQ(sprite3.maxFrame, 0);
    EXPECT_EQ(sprite3.spriteFrame, 0);

    EXPECT_EQ(sprite1 == sprite2, true);

}

TEST(testingPositionConstructors, DefalutCopyInitialize) {
    double x = 1;
    double y = 0;

    Position position1(x, y);

    EXPECT_EQ(position1.x, x);
    EXPECT_EQ(position1.y, y);

    Position position2(position1);

    EXPECT_EQ(position2.x, x);
    EXPECT_EQ(position2.y, y);

    Position position3;

    EXPECT_EQ(position3.x, 0);
    EXPECT_EQ(position3.y, 0);

    EXPECT_EQ(position1 == position2, true);
}

TEST(testingDefaultObjectConstructors, DefalutCopyInitialize) {

    int spriteID = 1;
    int spriteFrame = 2;
    int spriteAction = 3;
    int maxFrame = 10;

    DefaultObject defaultObject1(spriteID, spriteFrame, spriteAction, maxFrame);

    EXPECT_EQ(defaultObject1.sprite->spriteFrame, spriteFrame);
    EXPECT_EQ(defaultObject1.sprite->spriteID, spriteID);
    EXPECT_EQ(defaultObject1.sprite->spriteAction, spriteAction);
    EXPECT_EQ(defaultObject1.sprite->maxFrame, maxFrame);

    DefaultObject defaultObject2(defaultObject1);

    EXPECT_EQ(defaultObject2.sprite->spriteFrame, spriteFrame);
    EXPECT_EQ(defaultObject2.sprite->spriteID, spriteID);
    EXPECT_EQ(defaultObject2.sprite->spriteAction, spriteAction);
    EXPECT_EQ(defaultObject2.sprite->maxFrame, maxFrame);


    DefaultObject defaultObject3;

    EXPECT_EQ(defaultObject3.sprite, nullptr);

    Sprite sprite1(spriteID, spriteFrame, spriteAction, maxFrame);

    DefaultObject defaultObject4(sprite1);

    EXPECT_EQ(defaultObject4.sprite->spriteFrame, spriteFrame);
    EXPECT_EQ(defaultObject4.sprite->spriteID, spriteID);
    EXPECT_EQ(defaultObject4.sprite->spriteAction, spriteAction);
    EXPECT_EQ(defaultObject4.sprite->maxFrame, maxFrame);


}


TEST(testingMapConstructors, DefalutCopyInitialize) {

    int spriteID = 1;
    int spriteFrame = 2;
    int spriteAction = 0;
    int maxFrame = 0;

    Map map1(spriteID, spriteFrame, spriteAction, maxFrame);

    EXPECT_EQ(map1.sprite->spriteFrame, spriteFrame);
    EXPECT_EQ(map1.sprite->spriteID, spriteID);
    EXPECT_EQ(map1.sprite->spriteAction, 0);
    EXPECT_EQ(map1.sprite->maxFrame, 0);

    Map map2(spriteID);

    EXPECT_EQ(map2.sprite->spriteFrame, 0);
    EXPECT_EQ(map2.sprite->spriteID, spriteID);
    EXPECT_EQ(map2.sprite->spriteAction, 0);
    EXPECT_EQ(map2.sprite->maxFrame, 0);

    Map map3(map1);

    EXPECT_EQ(map3.sprite->spriteFrame, spriteFrame);
    EXPECT_EQ(map3.sprite->spriteID, spriteID);
    EXPECT_EQ(map3.sprite->spriteAction, 0);
    EXPECT_EQ(map3.sprite->maxFrame, 0);

    Sprite sprite1(spriteID, spriteFrame, spriteAction, maxFrame);

    Map map4(sprite1);

    EXPECT_EQ(map4.sprite->spriteFrame, spriteFrame);
    EXPECT_EQ(map4.sprite->spriteID, spriteID);
    EXPECT_EQ(map4.sprite->spriteAction, spriteAction);
    EXPECT_EQ(map4.sprite->maxFrame, maxFrame);

}


TEST(testingDefalutObjectConstructors, DefalutCopyInitialize) {

    std::pair<double, double> pair = make_pair(1, 2);
    Position position(1, 2);
    Sprite sprite(1, 2, 3, 4);

    GameObject gameObject1(position, pair, sprite);
    GameObject gameObject3(position, pair, sprite);


    EXPECT_EQ(*(gameObject1.sprite), sprite);
    EXPECT_EQ(*gameObject1.currentPosition, position);
    EXPECT_EQ(gameObject1.currentSpeed.first, pair.first);
    EXPECT_EQ(gameObject1.currentSpeed.second, pair.second);



    GameObject gameObject2;

    EXPECT_EQ((gameObject2.sprite), nullptr);
    EXPECT_EQ(gameObject2.currentPosition, nullptr);
    EXPECT_EQ(gameObject2.currentSpeed.first, 0);
    EXPECT_EQ(gameObject2.currentSpeed.second, 0);

    EXPECT_EQ(gameObject1 == gameObject3, true);


}

TEST(testingBulletConstructors, DefalutCopyInitialize) {

    std::pair<double, double> pair = make_pair(1, 2);
    Position position(1, 2);
    Sprite sprite(1, 2, 3, 4);
    int damage = 20;

    Bullet bullet1(position, pair, sprite, damage);
    Bullet bullet2(position, pair, sprite, damage);

    EXPECT_EQ(*(bullet1.sprite), sprite);
    EXPECT_EQ(*bullet1.currentPosition, position);
    EXPECT_EQ(bullet1.currentSpeed.first, pair.first);
    EXPECT_EQ(bullet1.currentSpeed.second, pair.second);
    EXPECT_EQ(bullet1.damage, damage);

    EXPECT_EQ(bullet1 == bullet2, true);

}


TEST(testingWeaponConstructors, DefalutCopyInitialize) {

    std::pair<double, double> pair = make_pair(1, 2);
    Position position(1, 2);
    Sprite sprite(1, 2, 3, 4);
    bool isPicked = true;
    int bulletCapacity = 20;
    double fireSpeed = 30;
    double timeForShoot = 0.1;

    Weapon weapon1(position, pair, sprite, isPicked, bulletCapacity, fireSpeed, timeForShoot);
    Weapon weapon2(position, pair, sprite, isPicked, bulletCapacity, fireSpeed, timeForShoot);

    EXPECT_EQ(*weapon1.sprite, sprite);
    EXPECT_EQ(*weapon1.currentPosition, position);
    EXPECT_EQ(weapon1.currentSpeed.first, pair.first);
    EXPECT_EQ(weapon1.currentSpeed.second, pair.second);
    EXPECT_EQ(weapon1.isPicked, isPicked);
    EXPECT_EQ(weapon1.bulletCapacity, bulletCapacity);
    EXPECT_EQ(weapon1.fireSpeed, fireSpeed);
    EXPECT_EQ(weapon1.timeForShot, timeForShoot);

    EXPECT_EQ(weapon1 == weapon2, true);


}

TEST(testingPlayerConstructors, DefalutCopyInitialize) {

    std::pair<double, double> pair = make_pair(1, 2);
    Position position(1, 2);
    Sprite sprite(1, 2, 3, 4);
    bool isPicked = true;
    int bulletCapacity = 20;
    double fireSpeed = 30;
    double timeForShoot = 0.1;

    int playerId = 0;
    int playerHp = 100;

    Weapon weapon1(position, pair, sprite, isPicked, bulletCapacity, fireSpeed, timeForShoot);
    Player player1(position, pair, sprite, playerId, playerHp, &weapon1);

    EXPECT_EQ(*player1.currentPosition, position);
    EXPECT_EQ(*player1.sprite, sprite);
    EXPECT_EQ(player1.currentSpeed.first, pair.first);
    EXPECT_EQ(player1.currentSpeed.second, pair.second);
    EXPECT_EQ(player1.playerId, playerId);
    EXPECT_EQ(player1.playerHp, playerHp);
    EXPECT_EQ(player1.playerWeapon, &weapon1);

}


