//
// Created by znujko on 09.11.2019.
//

#include "userDataHandler_test.h"

#define MOVE_SPEED 10
#define JUMP_SPEED 10


IMessage *TestInputHandler::handleInput() {
    auto *packageTest = new CLIENT_PACKAGE(currentCommand, currentTime, userId);
    return (IMessage*)(packageTest);
}

void TestInputHandler::add(Commands command, double _currentTime) {
    currentCommand = command;
    currentTime = _currentTime;
}

TestInputHandler::TestInputHandler(unsigned char mainId) : userId(mainId)  {


}

TestUserActivityQueue::TestUserActivityQueue(const Player &player) {

    mainPlayer = new Player(player);

}

TestUserActivityQueue::~TestUserActivityQueue() {

    delete mainPlayer;

    while (!activityQueue.empty())
        activityQueue.pop();

}

Player *TestUserActivityQueue::add(const Commands &newAct) {

    switch (newAct) {
        case Commands::LEFTBUTTON : {
            mainPlayer->currentSpeed.first = MOVE_SPEED;
        }
        case Commands::LEFTERASE : {
            mainPlayer->currentSpeed.first = 0;
        }
        case Commands::RIGHTBUTTON : {
            mainPlayer->currentSpeed.first = -MOVE_SPEED;
        }
        case Commands::RIGHTERASE : {
            mainPlayer->currentSpeed.first = 0;
        }
    }

    activityQueue.push(*(GameObject*)(mainPlayer));

    return mainPlayer;
}


bool TestUserActivityQueue::compare(GameObject checkAction) {

    if (((Player*)(&checkAction))->playerId != mainPlayer->playerId)
        return false;

    if (checkAction == activityQueue.front()) {
        activityQueue.pop();
        return true;
    } else {
        while (!activityQueue.empty()) {
            activityQueue.pop();
        }
        return false;
    }
}

void TestUserActivityQueue::deleteAction() {
    activityQueue.pop();
}

void TestUserActivityQueue::updatePlayerState(DefaultObject *updateInformation) {

    if(updateInformation)
    {
        Player *newPlayer = (Player*)(updateInformation);

        delete mainPlayer;

        mainPlayer = new Player(*newPlayer);
    }

}

TestUserActivityQueue::TestUserActivityQueue() {

    mainPlayer = new Player();

}
