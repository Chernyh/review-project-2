//
// Created by znujko on 06.11.2019.
//

#ifndef GLIPGAME_USERDATAHANDLER_H
#define GLIPGAME_USERDATAHANDLER_H

#include <string>
#include <queue>
#include "message.h"
#include "data.h"
#include "clientPackage.h"
#include "userDataHandler.h"


class TestInputHandler {
public :

    TestInputHandler(unsigned char mainId) ;

    IMessage *handleInput();

    void add(Commands command, double _currentTime);

    Commands currentCommand;

    double currentTime;

    unsigned char userId;

};


class TestUserActivityQueue {
public :

    explicit TestUserActivityQueue(const Player &player);

    explicit  TestUserActivityQueue();

    ~TestUserActivityQueue();

    Player* add(const Commands & newAct);

    bool compare(GameObject checkAction);

    void updatePlayerState(DefaultObject *updateInformation);

    std::queue<GameObject> activityQueue;

    Player *mainPlayer;

    void deleteAction();

};


#endif //GLIPGAME_USERDATAHANDLER_H
