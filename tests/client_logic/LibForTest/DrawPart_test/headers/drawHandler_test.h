//
// Created by znujko on 06.11.2019.
//

#ifndef GLIPGAME_DRAWHANDLERTEST_H
#define GLIPGAME_DRAWHANDLERTEST_H


#include "drawHandler.h"


class TestTime : public BasicTime{
public :

    TestTime();

    double calculateTime() override;

    double getCurrentTime() override;

    void startTime() override;

    double currentTime;

    double timeForDrawing;
};



class TestBasicPhysic : public DefPhysic {
public :
    void update(GameObject *&objectToUpdate,double time) override;
};


class TestDrawActivity : public BasicDraw {
public :

    ~TestDrawActivity();

    TestDrawActivity(std::shared_ptr<DefPhysic> physicHandler);

    void drawActivity(GameObject*& objectUpdate,double drawTime) override ;

    std::shared_ptr<DefPhysic> GUIPhysic;


};


#endif //GLIPGAME_DRAWHANDLERTEST_H
