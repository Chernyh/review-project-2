//
// Created by znujko on 08.11.2019.
//



#include <ctime>
#include "drawHandler_test.h"

#define DEFAULT_SPRITE 1
#define SPRITE_PER_ONE_TIME 1
#define TIME_FOR_ONE_TICK 1

double TestTime::calculateTime() {

    currentTime = clock();

    double returnTime = currentTime - timeForDrawing;

    timeForDrawing = currentTime;

    return returnTime;
}

void TestTime::startTime() {

    currentTime = clock();

    timeForDrawing = clock();

}

double TestTime::getCurrentTime() {

    currentTime = clock();

    return currentTime;
}

TestTime::TestTime() {

    currentTime = 0;
    timeForDrawing = 0;

}

void TestBasicPhysic::update(GameObject *&objectToUpdate, double time) {

    double speedX = 0, speedY = 0;
    std::tie(speedX, speedY) = objectToUpdate->currentSpeed;

    (objectToUpdate)->currentPosition->x += speedX * time;

    (objectToUpdate)->currentPosition->y += speedY * time;


}

void TestDrawActivity::drawActivity(GameObject *&objectUpdate, double timeForDrawing) {

    int currentTime = 0;
    while (currentTime < timeForDrawing) {

        if (objectUpdate->sprite->maxFrame != DEFAULT_SPRITE) {
            objectUpdate->sprite->spriteFrame += SPRITE_PER_ONE_TIME;
            objectUpdate->sprite->spriteFrame %= objectUpdate->sprite->maxFrame;
        }

        GUIPhysic->update(objectUpdate, TIME_FOR_ONE_TICK);


        currentTime += TIME_FOR_ONE_TICK;


    }


}

TestDrawActivity::TestDrawActivity(std::shared_ptr<DefPhysic> physicHandler)
: GUIPhysic(physicHandler) {


}

TestDrawActivity::~TestDrawActivity() {


}
