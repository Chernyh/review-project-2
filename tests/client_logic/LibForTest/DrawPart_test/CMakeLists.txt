cmake_minimum_required(VERSION 3.10.2)

project(drawPart_test)

add_library(${PROJECT_NAME} src/drawHandler_test.cpp)
add_library(sub::drawPart_test ALIAS ${PROJECT_NAME})


target_include_directories(${PROJECT_NAME}
        PUBLIC
        ${PROJECT_SOURCE_DIR}/headers

)

target_link_libraries(${PROJECT_NAME}
        sub::graphic
        sub::data
        sub::drawPart
        sub::dataStorage
        sub::types
        )