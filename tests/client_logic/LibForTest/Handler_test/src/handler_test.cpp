//
// Created by znujko on 10.12.2019.
//

#include "handler_test.h"



TestBaseHandler::TestBaseHandler() : next(nullptr), handledMap(nullptr) {

}

void TestBaseHandler::setNext(Handler *&nextHandler, std::shared_ptr<std::map<int, DefaultObject *>> _map) {

    next = nextHandler;
    handledMap = std::move(_map);

}


TestPlayerHandler::TestPlayerHandler() : TestBaseHandler() {

}

std::shared_ptr<std::map<int, DefaultObject *>> TestPlayerHandler::handle(UsingTypes objType) {

    if (objType == UsingTypes::PLAYER) {
        return handledMap;
    } else {
        if (next)
            return next->handle(objType);
        return nullptr;
    }

}


TestWeaponHandler::TestWeaponHandler() : TestBaseHandler() {

}

std::shared_ptr<std::map<int, DefaultObject *>> TestWeaponHandler::handle(UsingTypes objType) {

    if (objType == UsingTypes::WEAPON) {
        return handledMap;
    } else {
        if (next)
            return next->handle(objType);
        return nullptr;
    }

}

TestBulletHandler::TestBulletHandler() : TestBaseHandler() {

}

std::shared_ptr<std::map<int, DefaultObject *>> TestBulletHandler::handle(UsingTypes objType) {

    if (objType == UsingTypes::BULLET) {
        return handledMap;
    } else {
        if (next)
            return next->handle(objType);
        return nullptr;
    }
}


TestContainer::TestContainer(Handler *seqStart) {

    firstItem = seqStart;

}

std::shared_ptr<std::map<int, DefaultObject *>> TestContainer::handle(UsingTypes objType) {
    return firstItem->handle(objType);
}
