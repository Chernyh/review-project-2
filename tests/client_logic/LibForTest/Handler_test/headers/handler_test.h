//
// Created by znujko on 10.12.2019.
//

#ifndef MAIN_PROJECT_HANDLERTESTH
#define MAIN_PROJECT_HANDLERTEST_H


#include <map>
#include <memory>

#include "data.h"
#include "handler.h"

class TestContainer {

public :

    explicit TestContainer(Handler *seqStart);

    std::shared_ptr<std::map<int, DefaultObject *>> handle(UsingTypes objType);

    Handler *firstItem;

};


class TestBaseHandler : public Handler {

public :

    TestBaseHandler();


    void setNext(Handler *&nextHandler, std::shared_ptr<std::map<int, DefaultObject *>> _map) override;

    Handler *next;
    std::shared_ptr<std::map<int, DefaultObject *>> handledMap;

};


class TestPlayerHandler : public TestBaseHandler {

public :

    TestPlayerHandler();

    std::shared_ptr<std::map<int, DefaultObject *>> handle(UsingTypes objType) override;

};


class TestWeaponHandler : public TestBaseHandler {

public :

    TestWeaponHandler();

    std::shared_ptr<std::map<int, DefaultObject *>> handle(UsingTypes objType) override;

};

class TestBulletHandler : public TestBaseHandler {

public :

    TestBulletHandler();

    std::shared_ptr<std::map<int, DefaultObject *>> handle(UsingTypes objType) override;

};




#endif //MAIN_PROJECT_HANDLERTEST_H
