cmake_minimum_required(VERSION 3.10.2)

project(handler_test)

add_library(${PROJECT_NAME} src/handler_test.cpp)
add_library(sub::handler_test ALIAS ${PROJECT_NAME})

target_include_directories(${PROJECT_NAME}
        PUBLIC
        ${PROJECT_SOURCE_DIR}/headers
        )



target_link_libraries(${PROJECT_NAME} PRIVATE
        sub::handler
        sub::dataStorage
        sub::types
        sub::data
        )