//
// Created by znujko on 06.11.2019.
//

#ifndef GLIPGAME_TESTSERVERDATAHANDLER_H
#define GLIPGAME_TESTSERVERDATAHANDLER_H

#include <memory>
#include <queue>
#include "serverDataHandler.h"
#include "userDataHandler_test.h"
#include "dataHandler_test.h"
#include "serverHandler.h"
#include "packages.h"

class ActionContainer
{
public:
    void handle(server_packages::ServerPackagePart *package);
};


class TestAnswerFactory : public BasicAnswerFactory {
public :

    explicit TestAnswerFactory(std::shared_ptr<ActionContainer> containerInit,std::shared_ptr<TestRepositoryManager> managerInit);

    ~TestAnswerFactory();

    std::shared_ptr<IMessage> eventUpdate(std::shared_ptr<IMessage> Message) override;

    std::shared_ptr<IMessage> eventInit(std::shared_ptr<IMessage> Message) override;


    std::shared_ptr<TestRepositoryManager> manager;
    std::shared_ptr<ActionContainer> container;

};



#endif //GLIPGAME_TESTSERVERDATAHANDLER_H
