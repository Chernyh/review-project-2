//
// Created by znujko on 08.11.2019.
//

#include "serverDataHandler_test.h"
#include "serverHandler.h"

std::shared_ptr<IMessage> TestAnswerFactory::eventInit(std::shared_ptr<IMessage> Message) {


    std::shared_ptr<server_packages::ServerInitialPackage> initPackage = std::dynamic_pointer_cast<server_packages::ServerInitialPackage>(
            Message);

    Map *map = new Map(initPackage->mapID);


    manager->add(0, (DefaultObject *) (map), UsingTypes::MAP);
    manager->setMainUserId((int) initPackage->playerID);


    return Message;
}


std::shared_ptr<IMessage> TestAnswerFactory::eventUpdate(std::shared_ptr<IMessage> Message) {


    std::shared_ptr<server_packages::ServerUpdatePackage> initPackage = std::dynamic_pointer_cast<server_packages::ServerUpdatePackage>(
            Message);

    for (auto &i : initPackage->data) {

        container->handle((server_packages::ServerPackagePart *) (&*initPackage));


    }

    return Message;
}


TestAnswerFactory::TestAnswerFactory(std::shared_ptr<ActionContainer> containerInit,std::shared_ptr<TestRepositoryManager> managerInit) : manager(managerInit) , container(containerInit){



}


TestAnswerFactory::~TestAnswerFactory() {


}





