project(dataHandler_test)

add_library(${PROJECT_NAME} src/dataHandler_test.cpp)
add_library(sub::dataHandler_test ALIAS ${PROJECT_NAME})


target_include_directories(${PROJECT_NAME}
        PUBLIC
        ${PROJECT_SOURCE_DIR}/headers

        )

target_link_libraries(${PROJECT_NAME}
        sub::drawPart_test
        sub::dataStorage
        sub::dataHandler
        sub::handler_test
        sub::types
        )