//
// Created by znujko on 08.11.2019.
//

#ifndef MAIN_PROJECT_DATAHANDLER_H
#define MAIN_PROJECT_DATAHANDLER_H

#include <map>
#include <memory>
#include "dataHandler.h"
#include "handler_test.h"






class TestRepository : public DataInterface {
public :




    TestRepository(const int &userId,std::shared_ptr<SpriteGraphic> _initGraphic);

    ~TestRepository();

    void update(int idUpdate, DefaultObject *updatedObject, UsingTypes objType) override;

    void deleteData(int idDelete, UsingTypes objType) override;

    void add(int idAdd, DefaultObject *addObject, UsingTypes objType) override;

    DefaultObject *get(int idGet, UsingTypes objType) override;

    Player *getMainUser();

    void updateMainUser(DefaultObject *ptrMainUser);

    void setMainUserId(int setId);

    Map *map;

    int mainCharacterId;

    TestContainer *container;

    std::shared_ptr<std::map<int, DefaultObject *>> playerMap;
    std::shared_ptr<std::map<int, DefaultObject *>> bulletMap;
    std::shared_ptr<std::map<int, DefaultObject *>> weaponMap;
};


class TestRepositoryManager : public DataInterface {
public:

    explicit TestRepositoryManager(std::shared_ptr<TestRepository> repository);

    ~TestRepositoryManager();

    void update(int idUpdate, DefaultObject *updatedObject, UsingTypes objType) override;

    void deleteData(int idDelete, UsingTypes objType) override;

    void add(int idAdd, DefaultObject *addObject, UsingTypes objType) override;

    DefaultObject *get(int idGet, UsingTypes objType) override;

    Player *getMainUser();

    void updateMainUser(DefaultObject *ptrMainUser);

    void setMainUserId(int setId);

    std::shared_ptr<TestRepository> repositoryPointer;
};



#endif //MAIN_PROJECT_DATAHANDLER_H
