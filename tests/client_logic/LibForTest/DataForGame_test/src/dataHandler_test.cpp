//
// Created by znujko on 08.11.2019.
//

#include <memory>
#include "dataHandler_test.h"


void TestRepository::update(int idUpdate, DefaultObject *updatedObject, UsingTypes objType) {


    std::shared_ptr<std::map<int, DefaultObject *>> mapToAdd = container->handle(objType);

    if (mapToAdd) {
        mapToAdd->operator[](idUpdate) = updatedObject;
    } else {
        if (map)
            delete map;
        map = new Map(*((Map *) (updatedObject)));
    }

}


void TestRepository::deleteData(int idDelete, UsingTypes objType) {

    std::shared_ptr<std::map<int, DefaultObject *>> mapToDelete = container->handle(objType);

    if (mapToDelete) {

           } else {
        if (map)
            delete map;
        map = nullptr;
    }


}

void TestRepository::add(int idAdd, DefaultObject *addObject, UsingTypes objType) {


    std::shared_ptr<std::map<int, DefaultObject *>> mapToAdd = container->handle(objType);

    if (mapToAdd) {
        mapToAdd->operator[](idAdd) = addObject;
    } else {
        if (map)
            delete map;
        map = new Map(*((Map *) (addObject)));
    }


}

DefaultObject *TestRepository::get(int idGet, UsingTypes objType) {

    std::shared_ptr<std::map<int, DefaultObject *>> mapToGet = container->handle(objType);

    if (mapToGet) {

        return mapToGet->at(idGet);

    } else {
        return map;
    }


}


TestRepository::~TestRepository() {


}


Player *TestRepository::getMainUser() {
    if (mainCharacterId < 0) {
        return nullptr;
    }
    return (Player *) (playerMap->at(mainCharacterId));
}

TestRepository::TestRepository(const int &userId,
                               std::shared_ptr<SpriteGraphic> _initGraphic) {

    mainCharacterId = userId;

    map = nullptr;



    Handler *null;
    Handler *playerHandler = new TestPlayerHandler();
    Handler *weaponHandler = new TestWeaponHandler();
    Handler *bulletHandler = new TestBulletHandler();
    Handler *nullHandler = nullptr;

    playerHandler->setNext(weaponHandler, playerMap);
    weaponHandler->setNext(bulletHandler, weaponMap);
    bulletHandler->setNext(nullHandler, bulletMap);

    container = new TestContainer(playerHandler);

    std::map<int, DefaultObject *> pM;
    std::map<int, DefaultObject *> wM;
    std::map<int, DefaultObject *> bM;

    playerMap = std::make_shared<std::map<int, DefaultObject *>>(pM);
    weaponMap = std::make_shared<std::map<int, DefaultObject *>>(wM);
    bulletMap = std::make_shared<std::map<int, DefaultObject *>>(bM);


}

void TestRepository::updateMainUser(DefaultObject *ptrMainUser) {

    if (mainCharacterId != -1) {
        playerMap->erase(mainCharacterId);
        (*playerMap)[mainCharacterId] = new Player(*(Player *) (ptrMainUser));
    }

}

void TestRepository::setMainUserId(int setId) {

    mainCharacterId = setId;

}

TestRepositoryManager::TestRepositoryManager(std::shared_ptr<TestRepository> repository) {

    repositoryPointer = repository;

}

void TestRepositoryManager::update(int idUpdate, DefaultObject *updatedObject, UsingTypes objType) {

    repositoryPointer->update(idUpdate, updatedObject, objType);

}

void TestRepositoryManager::deleteData(int idDelete, UsingTypes objType) {

    repositoryPointer->deleteData(idDelete, objType);

}

void TestRepositoryManager::add(int idAdd, DefaultObject *addObject, UsingTypes objType) {

    repositoryPointer->add(idAdd, addObject, objType);

}

DefaultObject *TestRepositoryManager::get(int idGet, UsingTypes objType) {

    return repositoryPointer->get(idGet, objType);
}

Player *TestRepositoryManager::getMainUser() {
    return repositoryPointer->getMainUser();
}

void TestRepositoryManager::updateMainUser(DefaultObject *ptrMainUser) {

    repositoryPointer->updateMainUser(ptrMainUser);

}

TestRepositoryManager::~TestRepositoryManager() {

    repositoryPointer = nullptr;

}

void TestRepositoryManager::setMainUserId(int setId) {

    repositoryPointer->setMainUserId(setId);

}

