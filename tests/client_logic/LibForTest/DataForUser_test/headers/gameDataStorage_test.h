//
// Created by znujko on 06.11.2019.
//


#include <string>
#include <queue>
#include <utility>
#include "gameDataStorage.h"
#include "packageTypes.h"



class TestActionQueue : public DefActionQueue {
public :

    TestActionQueue();

    ~TestActionQueue();

    Commands popAction() override;

    void addAction(Commands stringAdded) override;

    std::queue<Commands> actionQueue;
};



