//
// Created by znujko on 07.11.2019.
//

#include "gameDataStorage_test.h"


Commands TestActionQueue::popAction() {

    if(actionQueue.empty())
    {
        return Commands  :: ZERO ;
    }

    Commands result = actionQueue.front();
    actionQueue.pop();
    return result;

}

void TestActionQueue::addAction(Commands stringAdded) {

    actionQueue.push(stringAdded);

}

TestActionQueue::TestActionQueue() {

    while(!actionQueue.empty())
        actionQueue.pop();

}

TestActionQueue::~TestActionQueue() {

    while(!actionQueue.empty())
        actionQueue.pop();

}


