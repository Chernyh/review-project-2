//
// Created by znujko on 06.11.2019.
//

#ifndef GLIPGAME_SERVERDATAHANDLER_H
#define GLIPGAME_SERVERDATAHANDLER_H


#include <memory>
#include <queue>
#include "dataHandler.h"
#include "packages.h"
#include "serverHandler.h"
#include "userDataHandler.h"


#include <memory>
#include <queue>
#include "dataHandler.h"
#include "packages.h"
#include "serverHandler.h"
#include "userDataHandler.h"


class FactoryPIMPL;

class BasicAnswerFactory {
public :
    virtual std::shared_ptr<IMessage> eventUpdate(std::shared_ptr<IMessage> Message) = 0;

    virtual std::shared_ptr<IMessage> eventInit(std::shared_ptr<IMessage> Message) = 0;



};

class AnswerFactory : public BasicAnswerFactory {
public :

    explicit AnswerFactory(std::shared_ptr<ActionContainer> containerInit,std::shared_ptr<RepositoryManager> managerInit);

    ~AnswerFactory();

    std::shared_ptr<IMessage> eventUpdate(std::shared_ptr<IMessage> Message) override;

    std::shared_ptr<IMessage> eventInit(std::shared_ptr<IMessage> Message) override;



private :

    FactoryPIMPL *factoryPimpl;


};

#endif //GLIPGAME_SERVERDATAHANDLER_H
