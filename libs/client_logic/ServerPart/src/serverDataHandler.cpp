#include <utility>
#include "serverHandler.h"
#include "serverDataHandler.h"


class FactoryPIMPL {
public :

    FactoryPIMPL(std::shared_ptr<ActionContainer> containerInit, std::shared_ptr<RepositoryManager> managerInit)
            : container(containerInit), manager(managerInit) {

    };

    std::shared_ptr<RepositoryManager> manager;
    std::shared_ptr<ActionContainer> container;
};

std::shared_ptr<IMessage> AnswerFactory::eventInit(std::shared_ptr<IMessage> Message) {


    std::shared_ptr<server_packages::ServerInitialPackage> initPackage = std::reinterpret_pointer_cast<server_packages::ServerInitialPackage>(
            Message);

    Map *map = new Map(initPackage->mapID);




    factoryPimpl->manager->add(0, (DefaultObject *) (map), UsingTypes::MAP);
    factoryPimpl->manager->setMainUserId((int) initPackage->playerID);


    return Message;
}




std::shared_ptr<IMessage> AnswerFactory::eventUpdate(std::shared_ptr<IMessage> Message) {


    //auto initPackage = (std::shared_ptr<server_packages::ServerUpdatePackage>)(Message);
    std::shared_ptr<server_packages::ServerUpdatePackage> initPackage = std::reinterpret_pointer_cast<server_packages::ServerUpdatePackage>(
            Message);


    for (auto &i : initPackage->data) {
        factoryPimpl->container->handle((server_packages::ServerPackagePart *) (&*i));
    }

    return Message;
}


AnswerFactory::AnswerFactory(std::shared_ptr<ActionContainer> containerInit,
                             std::shared_ptr<RepositoryManager> managerInit)  {

    factoryPimpl = new FactoryPIMPL(containerInit,managerInit);

}


AnswerFactory::~AnswerFactory() {

    delete factoryPimpl;

}

