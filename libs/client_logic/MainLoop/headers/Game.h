//
// Created by znujko on 09.11.2019.
//

#ifndef MAIN_PROJECT_GAME_H
#define MAIN_PROJECT_GAME_H

#include "gameDataStorage.h"
#include "serverHandler.h"
#include "serverDataHandler.h"
#include "connector.h"
#include "gameDataStorage.h"
#include "drawHandler.h"
#include "dataHandler.h"
#include "userDataHandler.h"
#include "types.h"
#include "packages.h"

class MainLogicPIMPL;

class LogicLoop {


public :

    explicit LogicLoop(std::shared_ptr<Connector> loop);
    ~LogicLoop();

    void StartLoop();
    static void* CreateGameWindow(void *_spriteGraphic);

private :
    MainLogicPIMPL *logicPimpl;
};


#endif //MAIN_PROJECT_GAME_H
