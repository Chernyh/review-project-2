//
// Created by znujko on 09.11.2019.
//


#include <pthread.h>
#include "Game.h"



class MainLogicPIMPL {
public :

    MainLogicPIMPL(std::shared_ptr<Connector> loop) {
        spriteGraphic = std::make_shared<SpriteQueue>();
        basicPhysic = std::make_shared<BasicPhysic>();
        drawActivity = std::make_shared<DrawActivity>(basicPhysic, spriteGraphic);
        repository = std::make_shared<Repository>(-1, drawHandler, spriteGraphic);

        drawHandler->setDrawPart(drawActivity);
        repositoryManager = std::make_shared<RepositoryManager>(repository);
        timeChecker = std::make_shared<Time>();
        actionQueue = std::make_shared<ActionQueue>();
        inputHandler = std::make_shared<InputHandler>(-1);
        userActivityQueue = std::make_shared<UserActivityQueue>();

        BasicHandler *weaponUpdateHandler = new WeaponUpdateHandle(repositoryManager);
        BasicHandler *bulletUpdateHandler = new BulletUpdateHandle(repositoryManager);
        BasicHandler *playerUpdateHandler = new PlayerUpdateHandle(repositoryManager,userActivityQueue);

        bulletUpdateHandler->setNext(playerUpdateHandler);
        playerUpdateHandler->setNext(weaponUpdateHandler);

        ActionHandler *updateHandler = new UpdateContainer();
        updateHandler->setSequence(bulletUpdateHandler);

        BasicHandler *weaponDeleteHandler = new WeaponDeleteHandle(repositoryManager);
        BasicHandler *bulletDeleteHandler = new BulletDeleteHandle(repositoryManager);
        BasicHandler *playerDeleteHandler = new PlayerDeleteHandle(repositoryManager);

        weaponDeleteHandler->setNext(bulletDeleteHandler);
        bulletDeleteHandler->setNext(playerDeleteHandler);

        ActionHandler *deleteHandler = new DeleteContainer();
        deleteHandler->setSequence(weaponDeleteHandler);

        updateHandler->setNext(deleteHandler);

        std::shared_ptr<ActionContainer> actionHandler = std::make_shared<ActionContainer>(updateHandler);

        answerFactory = std::make_shared<AnswerFactory>(
                actionHandler, repositoryManager);
        connector = loop;

        connector->registerEvent(std::bind(&AnswerFactory::eventInit, answerFactory, std::placeholders::_1), "Init");
        connector->registerEvent(std::bind(&AnswerFactory::eventUpdate, answerFactory, std::placeholders::_1),
                                 "Update");


        pthread_attr_init(&attr);

    };

    std::shared_ptr<SpriteQueue> spriteGraphic;
    std::shared_ptr<BasicPhysic> basicPhysic;
    std::shared_ptr<DrawActivity> drawActivity;
    std::shared_ptr<DrawHandler> drawHandler;
    std::shared_ptr<Repository> repository;
    std::shared_ptr<RepositoryManager> repositoryManager;
    std::shared_ptr<Time> timeChecker;
    std::shared_ptr<ActionQueue> actionQueue;
    std::shared_ptr<InputHandler> inputHandler;
    std::shared_ptr<UserActivityQueue> userActivityQueue;
    std::shared_ptr<AnswerFactory> answerFactory;
    std::shared_ptr<Connector> connector;
    pthread_attr_t attr;
    pthread_t tids[1];
};

LogicLoop::LogicLoop(std::shared_ptr<Connector> loop) {

    logicPimpl = new MainLogicPIMPL(loop);
    StartLoop();
    //  pthread_create(&logicPimpl->tids[0], &logicPimpl->attr, CreateGameWindow, logicPimpl->spriteGraphic.get());

}

LogicLoop::~LogicLoop() {

    pthread_join(logicPimpl->tids[0], NULL);


}

void *LogicLoop::CreateGameWindow(void *_spriteGraphic) {


    //SpriteGraphic *initPtr = (SpriteGraphic *) (_spriteGraphic);
    // GameWindow(initPtr); <- нету в shared GameWindow от Виктории Губановой

}


void LogicLoop::StartLoop() {

    Commands command;
    while (1) {

        while ((command = logicPimpl->actionQueue->popAction()) != Commands::ZERO) {
            logicPimpl->inputHandler->add(command, logicPimpl->timeChecker->getCurrentTime());
            std::shared_ptr<IMessage> message = std::make_shared<IMessage>(*logicPimpl->inputHandler->handleInput());
            logicPimpl->connector->sendMessage(message);
            logicPimpl->repositoryManager->updateMainUser(logicPimpl->userActivityQueue->add(command));
        }


        double timeDraw = logicPimpl->timeChecker->getCurrentTime();
        logicPimpl->drawHandler->draw(timeDraw);
        logicPimpl->userActivityQueue->updatePlayerState(logicPimpl->repositoryManager->getMainUser());
    }


}

