//
// Created by znujko on 10.12.2019.
//

#include "handler.h"




BaseHandler::BaseHandler() : next(nullptr), handledMap(nullptr) {

}

void BaseHandler::setNext(Handler *&nextHandler, std::shared_ptr<std::map<int, DefaultObject *>> _map) {

    next = nextHandler;
    handledMap = std::move(_map);

}


PlayerHandler::PlayerHandler() : BaseHandler() {

}

std::shared_ptr<std::map<int, DefaultObject *>> PlayerHandler::handle(UsingTypes objType) {

    if (objType == UsingTypes::PLAYER) {
        return handledMap;
    } else {
        if (next)
            return next->handle(objType);
        return nullptr;
    }

}


WeaponHandler::WeaponHandler() : BaseHandler() {

}

std::shared_ptr<std::map<int, DefaultObject *>> WeaponHandler::handle(UsingTypes objType) {

    if (objType == UsingTypes::WEAPON) {
        return handledMap;
    } else {
        if (next)
            return next->handle(objType);
        return nullptr;
    }

}

BulletHandler::BulletHandler() : BaseHandler() {

}

std::shared_ptr<std::map<int, DefaultObject *>> BulletHandler::handle(UsingTypes objType) {

    if (objType == UsingTypes::BULLET) {
        return handledMap;
    } else {
        if (next)
            return next->handle(objType);
        return nullptr;
    }
}


Container::Container(Handler *seqStart): firstItem(seqStart) {


}

std::shared_ptr<std::map<int, DefaultObject *>> Container::handle(UsingTypes objType) {
    return firstItem->handle(objType);
}