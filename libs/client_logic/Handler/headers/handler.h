//
// Created by znujko on 10.12.2019.
//

#ifndef PROJECT_HANDLER_H
#define PROJECT_HANDLER_H


#include <map>
#include <memory>
// #include "types.h" <- it's not linking . i have a question :C . Thus , a've already added sub::types to cmakelist , but it couldnt recognize it
#include "data.h"
#include "types.h"



class Handler {
public :
    virtual void setNext(Handler *&nextHandler, std::shared_ptr<std::map<int, DefaultObject *>> _map) = 0;

    virtual std::shared_ptr<std::map<int, DefaultObject *>> handle(UsingTypes objType) = 0;
};

class Container {

public :

    explicit Container(Handler *seqStart);

    std::shared_ptr<std::map<int, DefaultObject *>> handle(UsingTypes objType);

    Handler *firstItem;

};


class BaseHandler : public Handler {

public :

    BaseHandler();


    void setNext(Handler *&nextHandler, std::shared_ptr<std::map<int, DefaultObject *>> _map) override;

protected:
    std::shared_ptr<std::map<int, DefaultObject *>> handledMap;
    Handler *next;
};


class PlayerHandler : public BaseHandler {

public :

    PlayerHandler();

    std::shared_ptr<std::map<int, DefaultObject *>> handle(UsingTypes objType) override;

};


class WeaponHandler : public BaseHandler {

public :

    WeaponHandler();

    std::shared_ptr<std::map<int, DefaultObject *>> handle(UsingTypes objType) override;

};

class BulletHandler : public BaseHandler {

public :

    BulletHandler();

    std::shared_ptr<std::map<int, DefaultObject *>> handle(UsingTypes objType) override;

};







#endif //PROJECT_HANDLER_H
