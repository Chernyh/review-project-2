//
// Created by znujko on 10.12.2019.
//



#include "serverHandler.h"


void UpdateHandle::setNext(BasicHandler *&nextInit) {

    next = (UpdateHandle*)nextInit;

}

UpdateHandle::~UpdateHandle() {

    if (next)
        delete next;

}

UpdateHandle::UpdateHandle(std::shared_ptr<RepositoryManager> managerInit) : manager(managerInit) {

    next = nullptr;

}


WeaponUpdateHandle::WeaponUpdateHandle(std::shared_ptr<RepositoryManager> managerInit) : UpdateHandle(managerInit) {

}

void WeaponUpdateHandle::handle(server_packages::ServerPPUpdate *package) {

    if (package->objectType == server_packages::ObjectType::WEAPON) {

        server_packages::ServerPPUpdateBullet bullet = *((server_packages::ServerPPUpdateBullet *) (package));
        auto *bulletAdd = new Bullet(bullet.pos, std::make_pair(bullet.Vx, bullet.Vy), bullet.sprite, bullet.damage);

        manager->update(bullet.objectID, (DefaultObject *) (bulletAdd), UsingTypes::BULLET);


    } else {
        if (next)
            next->handle(package);
    }

}

BulletUpdateHandle::BulletUpdateHandle(std::shared_ptr<RepositoryManager> managerInit) : UpdateHandle(managerInit) {

}

void BulletUpdateHandle::handle(server_packages::ServerPPUpdate *package) {

    if (package->objectType == server_packages::ObjectType::BULLET) {
        server_packages::ServerPPUpdateWeapon weapon = *((server_packages::ServerPPUpdateWeapon *) (package));
        auto *weaponAdd = new Weapon(weapon.pos, std::make_pair(weapon.Vx, weapon.Vy), weapon.sprite, weapon.isPicked,
                                     (int) weapon.capacity, 0, 0);

        manager->update(weapon.objectID, (DefaultObject *) (weaponAdd), UsingTypes::WEAPON);

    } else {
        if (next)
            next->handle(package);
    }

}

PlayerUpdateHandle::PlayerUpdateHandle(std::shared_ptr<RepositoryManager> managerInit,
                                       std::shared_ptr<UserActivityQueue> compareInit) : UpdateHandle(
        managerInit) , compare(compareInit) {


}

void PlayerUpdateHandle::handle(server_packages::ServerPPUpdate *package) {

    if (package->objectType == server_packages::ObjectType::PLAYER) {

        server_packages::ServerPPUpdatePlayer player = *((server_packages::ServerPPUpdatePlayer *) (package));
        auto *weapon = (Weapon *) (manager->get(player.weaponID, UsingTypes::WEAPON));
        auto *playerAdd = new Player(player.pos, std::make_pair(player.Vx, player.Vy), player.sprite, player.objectID,
                                     player.HP, weapon);



    } else {
        if (next)
            next->handle(package);
    }

}


WeaponDeleteHandle::WeaponDeleteHandle(std::shared_ptr<RepositoryManager> managerInit) : DeleteHandle(
        managerInit) {

}

void WeaponDeleteHandle::handle(server_packages::ServerPPDelete *package) {

    if (package->objectType == server_packages::ObjectType::WEAPON) {
        manager->deleteData(package->objectID, UsingTypes::WEAPON);
    } else {
        if (next)
            next->handle(package);
    }

}

BulletDeleteHandle::BulletDeleteHandle(std::shared_ptr<RepositoryManager> managerInit) : DeleteHandle(managerInit) {

}

void BulletDeleteHandle::handle(server_packages::ServerPPDelete *package) {

    if (package->objectType == server_packages::ObjectType::BULLET) {
        manager->deleteData(package->objectID, UsingTypes::BULLET);
    } else {
        if (next)
            next->handle(package);
    }

}


PlayerDeleteHandle::PlayerDeleteHandle(std::shared_ptr<RepositoryManager> managerInit) : DeleteHandle(managerInit) {

}

void PlayerDeleteHandle::handle(server_packages::ServerPPDelete *package) {

    if (package->objectType == server_packages::ObjectType::PLAYER) {
        manager->deleteData(package->objectID, UsingTypes::PLAYER);
    } else {
        if (next)
            next->handle(package);
    }

}

DeleteHandle::~DeleteHandle() {

    if (next)
        delete next;

}

DeleteHandle::DeleteHandle(std::shared_ptr<RepositoryManager> managerInit) : manager(managerInit) {

}


void DeleteHandle::setNext(BasicHandler *&nextInit) {

    next = (DeleteHandle*)nextInit;

}

DeleteContainer::~DeleteContainer() {

    delete sequence;

    if (next)
        delete next;

}

DeleteContainer::DeleteContainer() : next(nullptr), sequence(nullptr) {

}

void DeleteContainer::setSequence(BasicHandler *seqInit) {

    sequence = (DeleteServerHandler*)seqInit;

}

void DeleteContainer::handle(server_packages::ServerPackagePart *package) {

    if (package->packageType == server_packages::ServerPackagePartType::DELETE) {

        sequence->handle((server_packages::ServerPPDelete *) package);

    } else {
        if (next)
            next->handle(package);
    }

}

void DeleteContainer::setNext(ActionHandler *&nextInit) {

    next = nextInit;

}

UpdateContainer::~UpdateContainer() {

    delete sequence;

    if (next)
        delete next;

}

UpdateContainer::UpdateContainer() : next(nullptr), sequence(nullptr) {

}

void UpdateContainer::setNext(ActionHandler *&nextInit) {

    next = nextInit;

}

void UpdateContainer::setSequence(BasicHandler *seqInit) {

    sequence = (UpdateServerHandler*)seqInit;
}

void UpdateContainer::handle(server_packages::ServerPackagePart *package) {

    if (package->packageType == server_packages::ServerPackagePartType::UPDATE) {

        sequence->handle((server_packages::ServerPPUpdate *) package);

    } else {
        if (next)
            next->handle(package);
    }

}

ActionContainer::ActionContainer(ActionHandler *initSeq) : sequence(initSeq) {

}

void ActionContainer::handle(server_packages::ServerPackagePart *package) {

    sequence->handle(package);

}
