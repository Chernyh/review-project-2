//
// Created by znujko on 10.12.2019.
//

#ifndef MAIN_PROJECT_HANDLER_H
#define MAIN_PROJECT_HANDLER_H


#include <functional>

#include "types.h"
#include "packages.h"
#include "dataHandler.h"
#include "userDataHandler.h"


class BasicHandler
{
public :

    virtual ~BasicHandler() = 0;

    virtual void setNext(BasicHandler *&nextInit) = 0 ;
};

class UpdateServerHandler : public BasicHandler
{

public :
    virtual void handle(server_packages::ServerPPUpdate *package) = 0 ;

};

class UpdateHandle : public UpdateServerHandler
{


public :

    ~UpdateHandle() override;

    explicit UpdateHandle(std::shared_ptr<RepositoryManager> managerInit);

    void setNext(BasicHandler *&nextInit) override ;

    virtual void handle(server_packages::ServerPPUpdate *package) = 0 ;


protected:
    std::shared_ptr<RepositoryManager> manager;
    UpdateHandle *next;
};


class WeaponUpdateHandle : public UpdateHandle
{
public :
    explicit WeaponUpdateHandle(std::shared_ptr<RepositoryManager> managerInit);
    void handle(server_packages::ServerPPUpdate *package) override ;
};


class BulletUpdateHandle : public UpdateHandle
{
public :
    explicit BulletUpdateHandle(std::shared_ptr<RepositoryManager> managerInit);
    void handle(server_packages::ServerPPUpdate *package) override ;
};


class PlayerUpdateHandle : public UpdateHandle
{
public :
    PlayerUpdateHandle(std::shared_ptr<RepositoryManager> managerInit,std::shared_ptr<UserActivityQueue> compareInit);
    void handle(server_packages::ServerPPUpdate *package) override ;
private :
    std::shared_ptr<UserActivityQueue> compare;
};





class DeleteServerHandler : public BasicHandler
{

public :

    virtual void handle(server_packages::ServerPPDelete *package) = 0 ;

};

class DeleteHandle : public DeleteServerHandler
{


public :

    ~DeleteHandle() override;

    explicit DeleteHandle(std::shared_ptr<RepositoryManager> managerInit);

    void setNext(BasicHandler *&nextInit) override ;

    virtual void handle(server_packages::ServerPPDelete *package) = 0 ;


protected:
    std::shared_ptr<RepositoryManager> manager;
    DeleteHandle *next;
};

class WeaponDeleteHandle : public DeleteHandle
{
public :
    explicit WeaponDeleteHandle(std::shared_ptr<RepositoryManager> managerInit);
    void handle(server_packages::ServerPPDelete *package) override ;
};


class BulletDeleteHandle : public DeleteHandle
{
public :
    explicit BulletDeleteHandle(std::shared_ptr<RepositoryManager> managerInit);
    void handle(server_packages::ServerPPDelete *package) override ;
};


class PlayerDeleteHandle : public DeleteHandle
{
public :
    explicit PlayerDeleteHandle(std::shared_ptr<RepositoryManager> managerInit);
    void handle(server_packages::ServerPPDelete *package) override ;
};


class ActionHandler
{
public :

    virtual ~ActionHandler() = 0;

    virtual void handle(server_packages::ServerPackagePart *package) = 0;

    virtual void setNext(ActionHandler *&nextInit) = 0;

    virtual void setSequence(BasicHandler *seqInit) = 0;
};

class DeleteContainer : public ActionHandler
{
public :
    ~DeleteContainer() override ;

    explicit DeleteContainer();

    void setNext(ActionHandler *&nextInit) override ;

    void setSequence(BasicHandler *seqInit) override ;

    void handle(server_packages::ServerPackagePart *package) override;


private :

    DeleteServerHandler *sequence;

    ActionHandler *next;

};

class UpdateContainer : public ActionHandler
{
public :
    ~UpdateContainer() override ;

    explicit UpdateContainer();

    void setNext(ActionHandler *&nextInit) override ;

    void setSequence(BasicHandler *seqInit) override ;

    void handle(server_packages::ServerPackagePart *package) override;


private :

    UpdateServerHandler *sequence;

    ActionHandler *next;

};

class ActionContainer
{
public :

    explicit ActionContainer(ActionHandler *initSeq);

    void handle(server_packages::ServerPackagePart *package);
private :
    ActionHandler *sequence;
};






#endif //MAIN_PROJECT_HANDLER_H
