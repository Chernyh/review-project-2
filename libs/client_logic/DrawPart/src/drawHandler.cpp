//
// Created by znujko on 08.11.2019.
//


#include "drawHandler.h"

#define DEFAULT_SPRITE 1
#define SPRITE_PER_ONE_TIME 1
#define TIME_FOR_ONE_TICK 1

double Time::calculateTime() {

    currentTime = clock();

    double returnTime = currentTime - timeForDrawing;

    timeForDrawing = currentTime;

    return returnTime;
}

void Time::startTime() {

    currentTime = clock();

    timeForDrawing = clock();

}

double Time::getCurrentTime() {
    currentTime = clock();
    return currentTime;
}

Time::Time() {

    currentTime = 0;
    timeForDrawing = 0;

}

class DrawPIMPL {
public :

    DrawPIMPL(std::shared_ptr<DefPhysic> physicHandler, std::shared_ptr<SpriteQueue> ptrGraphic) : GUIPhysic(
            physicHandler), spriteUpdater(ptrGraphic) {

    };

    std::shared_ptr<DefPhysic> GUIPhysic;

    std::shared_ptr<SpriteQueue> spriteUpdater;
};


void BasicPhysic::update(GameObject *&objectToUpdate, double time) {

    double speedX = 0, speedY = 0;
    std::tie(speedX, speedY) = objectToUpdate->currentSpeed;

    (objectToUpdate)->currentPosition->x += speedX * time;

    (objectToUpdate)->currentPosition->y += speedY * time;


}

void DrawActivity::drawActivity(GameObject *&objectUpdate, double timeForDrawing) {

    int currentTime = 0;
    while (currentTime < timeForDrawing) {

        if (objectUpdate->sprite->maxFrame!= DEFAULT_SPRITE) {
            objectUpdate->sprite->spriteFrame += SPRITE_PER_ONE_TIME;
            objectUpdate->sprite->spriteFrame %= objectUpdate->sprite->maxFrame;
        }

        drawPimpl->GUIPhysic->update(objectUpdate, TIME_FOR_ONE_TICK);

        drawPimpl->spriteUpdater->addAction(ActionsUI::UPDATESPRITE, objectUpdate->drawSprite, *objectUpdate->sprite,
                                            *objectUpdate->currentPosition);

        currentTime += TIME_FOR_ONE_TICK;


    }


}

DrawActivity::DrawActivity(std::shared_ptr<DefPhysic> physicHandler, std::shared_ptr<SpriteQueue> ptrGraphic) {

    drawPimpl = new DrawPIMPL(physicHandler, ptrGraphic);

}

DrawActivity::~DrawActivity() {

    delete drawPimpl;


}

