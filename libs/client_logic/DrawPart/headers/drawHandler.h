//
// Created by znujko on 06.11.2019.
//

#ifndef GLIPGAME_DRAWHANDLER_H
#define GLIPGAME_DRAWHANDLER_H


#include <string>
#include <map>
#include <memory>
#include <queue>


#include "gameDataStorage.h"
#include "data.h"

class DrawPIMPL;

class BasicTime {
public :
    virtual double calculateTime() = 0;

    virtual double getCurrentTime() = 0;

    virtual void startTime() = 0;
};

class Time : public BasicTime {
public :

    Time();

    double calculateTime() override;

    double getCurrentTime() override;

    void startTime() override ;

private :

    double currentTime;

    double timeForDrawing;
};

class DefPhysic {
public :
    virtual void update(GameObject *&objectToUpdate, double time) = 0;
};

class BasicPhysic : public DefPhysic {
public :
    void update(GameObject *&objectToUpdate, double time) override;
};


class BasicDraw {
public :
    virtual void drawActivity(GameObject *&objectOpdate, double drawTime) = 0;
};

class DrawActivity : public BasicDraw {
public :

    ~DrawActivity();

    DrawActivity(std::shared_ptr<DefPhysic> physicHandler, std::shared_ptr<SpriteQueue> ptrGraphic);

    void drawActivity(GameObject *&objectUpdate, double drawTime) override;

private :

    DrawPIMPL *drawPimpl;


};


#endif //GLIPGAME_DRAWHANDLER_H
