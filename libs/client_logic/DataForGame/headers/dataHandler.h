//
// Created by znujko on 06.11.2019.
//

#ifndef GLIPDefault_DATAHANDLER_H
#define GLIPDefault_DATAHANDLER_H

#include <map>
#include <memory>

#include "handler.h"
#include "drawHandler.h"
#include "graphic.h"
#include "gameDataStorage.h"
#include "UITypes.h"

class DrawPIMPL;
class RepositoryPIMPL;

class DataInterface {
public :
    virtual void update(int idUpdate, DefaultObject *updatedObject, UsingTypes objType) = 0;

    virtual void deleteData(int idDelete, UsingTypes objType) = 0;

    virtual void add(int idAdd, DefaultObject *addObject, UsingTypes objType) = 0;

    virtual DefaultObject *get(int idGet, UsingTypes objType) = 0;
};


class DrawHandler {

public :

    explicit DrawHandler(std::shared_ptr<std::map<int, DefaultObject *>> &_playerMap,
                         std::shared_ptr<std::map<int, DefaultObject *>> &_bulletMap,
                         std::shared_ptr<std::map<int, DefaultObject *>> &_weaponMap);

    ~DrawHandler();

    explicit DrawHandler(DrawHandler *cpyHandler);

    void setDrawPart(std::shared_ptr<BasicDraw> drawPartSet);

    void draw(double time);

private :

    DrawPIMPL *drawPimpl;

};


class Repository : public DataInterface {
public :
    Repository(const int &userId, std::shared_ptr<DrawHandler> &initHandler, std::shared_ptr<SpriteQueue> initQueue);

    ~Repository();

    void update(int idUpdate, DefaultObject *updatedObject, UsingTypes objType) override;

    void deleteData(int idDelete, UsingTypes objType) override;

    void add(int idAdd, DefaultObject *addObject, UsingTypes objType) override;

    DefaultObject *get(int idGet, UsingTypes objType) override;

    Player *getMainUser();

    void updateMainUser(DefaultObject *ptrMainUser);

    void setMainUserId(int setId);

private :

    RepositoryPIMPL *repPimpl;

};


class RepositoryManager : public DataInterface {
public:

    explicit RepositoryManager(std::shared_ptr<Repository> repository);

    ~RepositoryManager();

    void update(int idUpdate, DefaultObject *updatedObject, UsingTypes objType) override;

    void deleteData(int idDelete, UsingTypes objType) override;

    void add(int idAdd, DefaultObject *addObject, UsingTypes objType) override;

    DefaultObject *get(int idGet, UsingTypes objType) override;

    Player *getMainUser();

    void updateMainUser(DefaultObject *ptrMainUser);

    void setMainUserId(int setId);

private :

    std::shared_ptr<Repository> repositoryPointer;
};

#endif //GLIPDefault_DATAHANDLER_H
