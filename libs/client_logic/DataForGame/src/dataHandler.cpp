#include "dataHandler.h"


class DrawPIMPL {
public :
    DrawPIMPL(std::shared_ptr<std::map<int, DefaultObject *>> &_playerMap,
              std::shared_ptr<std::map<int, DefaultObject *>> &_bulletMap,
              std::shared_ptr<std::map<int, DefaultObject *>> &_weaponMap) : playerMap(_playerMap),
                                                                             bulletMap(_bulletMap),
                                                                             weaponMap(_weaponMap) {

        playerMap = _playerMap;


    };

    void SetDrawPart(std::shared_ptr<BasicDraw> drawPartSet) {
        drawPart = drawPartSet;
    };

    std::shared_ptr<BasicDraw> drawPart;

    std::shared_ptr<std::map<int, DefaultObject *>> playerMap;
    std::shared_ptr<std::map<int, DefaultObject *>> bulletMap;
    std::shared_ptr<std::map<int, DefaultObject *>> weaponMap;
};

class RepositoryPIMPL {
public:

    RepositoryPIMPL(const int &userId, std::shared_ptr<DrawHandler> &initHandler,
                    std::shared_ptr<SpriteQueue> initQueue) : mainCharacterId(
            userId), map(nullptr), spriteQueue(initQueue) {

        Handler *null;

        Handler *playerHandler = new PlayerHandler();
        Handler *weaponHandler = new WeaponHandler();
        Handler *bulletHandler = new BulletHandler();

        playerHandler->setNext(weaponHandler, playerMap);
        weaponHandler->setNext(bulletHandler, weaponMap);
        bulletHandler->setNext(null, bulletMap);


        container = new Container(playerHandler);

        std::map<int, DefaultObject *> pM;
        std::map<int, DefaultObject *> wM;
        std::map<int, DefaultObject *> bM;

        playerMap = std::make_shared<std::map<int, DefaultObject *>>(pM);
        weaponMap = std::make_shared<std::map<int, DefaultObject *>>(wM);
        bulletMap = std::make_shared<std::map<int, DefaultObject *>>(bM);

        initHandler = std::make_shared<DrawHandler>(playerMap, bulletMap, weaponMap);
    };


    std::shared_ptr<SpriteQueue> spriteQueue;

    Map *map;

    int mainCharacterId;

    Container *container;
    std::shared_ptr<std::map<int, DefaultObject *>> playerMap;
    std::shared_ptr<std::map<int, DefaultObject *>> bulletMap;
    std::shared_ptr<std::map<int, DefaultObject *>> weaponMap;
};

void Repository::update(int idUpdate, DefaultObject *updatedObject, UsingTypes objType) {

    std::shared_ptr<std::map<int, DefaultObject *>> mapToAdd = repPimpl->container->handle(objType);

    if (mapToAdd) {
        repPimpl->spriteQueue->addAction(ActionsUI::UPDATESPRITE,
                                         ((GameObject *) mapToAdd->operator[](idUpdate))->drawSprite,
                                         *mapToAdd->operator[](idUpdate)->sprite, Position());
        if(!((GameObject *) mapToAdd->operator[](idUpdate))->drawSprite)
        {
            ((GameObject *) mapToAdd->operator[](idUpdate))->drawSprite = repPimpl->spriteQueue->popGameSprite();
        }

        mapToAdd->operator[](idUpdate) = updatedObject;
    } else {
        if (repPimpl->map)
            delete repPimpl->map;
        repPimpl->map = new Map(*((Map *) (updatedObject)));
    }

}


void Repository::deleteData(int idDelete, UsingTypes objType) {

    std::shared_ptr<std::map<int, DefaultObject *>> mapToDelete = repPimpl->container->handle(objType);

    if (mapToDelete) {
        GameSprite *gs;
        repPimpl->spriteQueue->addAction(ActionsUI::DELETESPRITE,
                                         ((GameObject *) mapToDelete->operator[](idDelete))->drawSprite,
                                         *mapToDelete->operator[](idDelete)->sprite, Position());
        mapToDelete->erase(idDelete);
    } else {
        if (repPimpl->map)
            delete repPimpl->map;
        repPimpl->map = nullptr;
    }
}

void Repository::add(int idAdd, DefaultObject *addObject, UsingTypes objType) {

    if (!addObject) {
        std::shared_ptr<std::map<int, DefaultObject *>> mapToAdd = repPimpl->container->handle(objType);

        if (mapToAdd) {
            repPimpl->spriteQueue->addAction(ActionsUI::UPDATESPRITE,
                                             ((GameObject *) mapToAdd->operator[](idAdd))->drawSprite,
                                             *mapToAdd->operator[](idAdd)->sprite, Position());
            if(!((GameObject *) mapToAdd->operator[](idAdd))->drawSprite)
            {
                ((GameObject *) mapToAdd->operator[](idAdd))->drawSprite = repPimpl->spriteQueue->popGameSprite();
            }
            mapToAdd->operator[](idAdd) = addObject;
        } else {
            if (repPimpl->map)
                delete repPimpl->map;
            repPimpl->map = new Map(*((Map *) (addObject)));
        }
    }

}

DefaultObject *Repository::get(int idGet, UsingTypes objType) {

    std::shared_ptr<std::map<int, DefaultObject *>> mapToGet = repPimpl->container->handle(objType);

    if (mapToGet) {

        return mapToGet->at(idGet);

    }
    return repPimpl->map;


}


Repository::~Repository() {

    if (repPimpl->map) {
        delete repPimpl->map;
    }

    repPimpl->playerMap->clear();
    repPimpl->bulletMap->clear();
    repPimpl->weaponMap->clear();

}


Player *Repository::getMainUser() {
    if (repPimpl->mainCharacterId < 0) {
        return nullptr;
    }
    return (Player *) (repPimpl->playerMap->at(repPimpl->mainCharacterId));
}

Repository::Repository(const int &userId, std::shared_ptr<DrawHandler> &initHandler,
                       std::shared_ptr<SpriteQueue> initQueue
) {

    repPimpl = new RepositoryPIMPL(userId, initHandler, initQueue);

}

void Repository::updateMainUser(DefaultObject *ptrMainUser) {


    if (repPimpl->mainCharacterId != -1) {
        repPimpl->playerMap->erase(repPimpl->mainCharacterId);
        (*repPimpl->playerMap)[repPimpl->mainCharacterId] = new Player(*(Player *) (ptrMainUser));
    }


}

void Repository::setMainUserId(int setId) {

    repPimpl->mainCharacterId = setId;


}

RepositoryManager::RepositoryManager(std::shared_ptr<Repository> repository) {

    repositoryPointer = repository;

}

void RepositoryManager::update(int idUpdate, DefaultObject *updatedObject, UsingTypes objType) {

    repositoryPointer->update(idUpdate, updatedObject, objType);

}

void RepositoryManager::deleteData(int idDelete, UsingTypes objType) {

    repositoryPointer->deleteData(idDelete, objType);

}

void RepositoryManager::add(int idAdd, DefaultObject *addObject, UsingTypes objType) {

    repositoryPointer->add(idAdd, addObject, objType);

}

DefaultObject *RepositoryManager::get(int idGet, UsingTypes objType) {

    return repositoryPointer->get(idGet, objType);
}

Player *RepositoryManager::getMainUser() {
    return repositoryPointer->getMainUser();
}

void RepositoryManager::updateMainUser(DefaultObject *ptrMainUser) {

    repositoryPointer->updateMainUser(ptrMainUser);

}

RepositoryManager::~RepositoryManager() {

}

void RepositoryManager::setMainUserId(int setId) {

    repositoryPointer->setMainUserId(setId);

}


void DrawHandler::draw(double time) {

    for (auto &itPlayer : *drawPimpl->playerMap) {
        auto *drawValue = (GameObject *) ((itPlayer).second);
        if (drawValue->currentSpeed.second != 0 && drawValue->currentSpeed.first != 0)
            drawPimpl->drawPart->drawActivity(drawValue, time);
    }

    for (auto &itPlayer : *drawPimpl->weaponMap) {
        auto *drawValue = (GameObject *) ((itPlayer).second);
        if (drawValue->currentSpeed.second != 0 && drawValue->currentSpeed.first != 0)
            drawPimpl->drawPart->drawActivity(drawValue, time);
    }

    for (auto &itPlayer : *drawPimpl->bulletMap) {
        auto *drawValue = (GameObject *) (itPlayer.second);
        if (drawValue->currentSpeed.second != 0 && drawValue->currentSpeed.first != 0)
            drawPimpl->drawPart->drawActivity(drawValue, time);
    }

}

void DrawHandler::setDrawPart(std::shared_ptr<BasicDraw> drawPartSet) {

    drawPimpl->SetDrawPart(drawPartSet);

}

DrawHandler::DrawHandler(DrawHandler *cpyHandler) {

    drawPimpl = new DrawPIMPL(cpyHandler->drawPimpl->playerMap, cpyHandler->drawPimpl->bulletMap,
                              cpyHandler->drawPimpl->weaponMap);
    drawPimpl->SetDrawPart(cpyHandler->drawPimpl->drawPart);

}

DrawHandler::~DrawHandler() {

    delete drawPimpl;

}

DrawHandler::DrawHandler(std::shared_ptr<std::map<int, DefaultObject *>> &_playerMap,
                         std::shared_ptr<std::map<int, DefaultObject *>> &_bulletMap,
                         std::shared_ptr<std::map<int, DefaultObject *>> &_weaponMap) {

    drawPimpl = new DrawPIMPL(_playerMap, _bulletMap, _weaponMap);

}



