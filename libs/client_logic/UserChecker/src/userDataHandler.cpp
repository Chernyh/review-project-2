//
// Created by znujko on 09.11.2019.
//

#include "userDataHandler.h"

#define MOVE_SPEED 10
#define JUMP_SPEED 10


IMessage *InputHandler::handleInput() {
    auto *packageTest = new CLIENT_PACKAGE(currentCommand, currentTime, userId);
    return (IMessage*)(packageTest);
}

void InputHandler::add(Commands command, double _currentTime) {
    currentCommand = command;
    currentTime = _currentTime;
}

InputHandler::InputHandler(unsigned char mainId) : userId(mainId) {

}

UserActivityQueue::UserActivityQueue(const Player &player) : mainPlayer(new Player(player)) {


}

UserActivityQueue::~UserActivityQueue() {

    delete mainPlayer;

    while (!activityQueue.empty())
        activityQueue.pop();

}

Player *UserActivityQueue::add(const Commands &newAct) {

    switch (newAct) {
        case Commands::LEFTBUTTON : {
            mainPlayer->currentSpeed.first = MOVE_SPEED;
        }
        case Commands::LEFTERASE : {
            mainPlayer->currentSpeed.first = 0;
        }
        case Commands::RIGHTBUTTON : {
            mainPlayer->currentSpeed.first = -MOVE_SPEED;
        }
        case Commands::RIGHTERASE : {
            mainPlayer->currentSpeed.first = 0;
        }
    }

    activityQueue.push(*(GameObject*)(mainPlayer));

    return mainPlayer;
}


bool UserActivityQueue::compare(GameObject checkAction) {

    if (((Player*)(&checkAction))->playerId != mainPlayer->playerId)
        return false;

    if (checkAction == activityQueue.front()) {
        activityQueue.pop();
        return true;
    } else {
        while (!activityQueue.empty()) {
            activityQueue.pop();
        }
        return false;
    }
}

void UserActivityQueue::deleteAction() {
    activityQueue.pop();
}

void UserActivityQueue::updatePlayerState(DefaultObject *updateInformation) {

    if(updateInformation)
    {
        Player *newPlayer = (Player*)(updateInformation);

        delete mainPlayer;

        mainPlayer = new Player(*newPlayer);
    }



}

UserActivityQueue::UserActivityQueue() {

    mainPlayer = new Player();

}

