//
// Created by znujko on 06.11.2019.
//

#ifndef GLIPGAME_USERDATAHANDLER_H
#define GLIPGAME_USERDATAHANDLER_H


#include <string>
#include <queue>

#include "message.h"
#include "packageTypes.h"
#include "clientPackage.h"
#include "data.h"


class BaseInputHandler
{
public :

    virtual IMessage *handleInput() = 0;

    virtual void add(Commands command, double _currentTime) = 0;

};

class InputHandler : public BaseInputHandler {
public :

    explicit InputHandler(unsigned char mainId);

    IMessage *handleInput() override ;

    void add(Commands command, double _currentTime) override ;

private :

    Commands currentCommand;

    double currentTime;

    unsigned char userId;

};


class UserActivityQueue {
public :

    explicit UserActivityQueue(const Player &player);

    explicit UserActivityQueue();

    ~UserActivityQueue();

    Player* add(const Commands & newAct);

    bool compare(GameObject checkAction);

    void updatePlayerState(DefaultObject *updateInformation);

private :

    std::queue<GameObject> activityQueue;

    Player *mainPlayer;

    void deleteAction();

};

#endif //GLIPGAME_USERDATAHANDLER_H
