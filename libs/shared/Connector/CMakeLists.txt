cmake_minimum_required(VERSION 3.10.2)

project(ServerConnector)

add_library(${PROJECT_NAME} src/connector.cpp)
add_library(sub::serverConnector ALIAS ${PROJECT_NAME})


target_include_directories(${PROJECT_NAME}
        PUBLIC
        ${PROJECT_SOURCE_DIR}/headers
        )
target_link_libraries(${PROJECT_NAME}
        sub::message
        )

