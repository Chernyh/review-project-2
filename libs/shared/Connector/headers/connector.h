//
// Created by znujko on 28.11.2019.
//

#ifndef MAIN_PROJECT_CONNECTOR_H
#define MAIN_PROJECT_CONNECTOR_H

#include <memory>
#include <functional>
#include "message.h"
class ConnectorImpl;
class EventLoop;
class Timer;

/**
		Класс осуществляющий взаимодействие с Циклом событй. Он нужен для того, чтобы
		предоставить возможность отправлять сообщения и регистрировать собственные обработчики,
		при этом не допуская до экземпляра EventLoop, иначе другая часть системы, сможет остановить
		работу Цикла событий, удалять чужие обрабочики и события.
	*/
class Connector {
public:
    Connector(std::shared_ptr<EventLoop> loop_ptr, int id);
    Connector(Connector && obj);
    Connector &&operator=(Connector &&obj);

    Connector(const Connector&) = delete;
    Connector operator=(const Connector&) = delete;
    /**
        \brief Отправка сообщений в очередь событий
        \param[in] message умный указатель на класс, который реализует интерфейс IMessage

        Для того, чтобы отправлять сообщения, необходимо взять собственный класс-контейнер,
        который будет содержать данные для отправки в другу часть системы. Отнаследовать его от
        класса IMessage и реализовать метод getType. Тип сообщеня должен совпадать с типом события,
        который должен реагировать на это сообщение.
    */
    bool sendMessage(std::shared_ptr<IMessage> message);
    /**
        \brief Регистрация обработчика на определенное событие.
        \param[in] handler указатель на функцию обработчик, для указания
        метода класса воспользуйтесь std::bind

        Регистрация обработчика на определенное событие.  Обработчиков может быть несколько.
        Для регистрации нескольких обработчиков вызовите несколько раз этот метод с разными указателями
        на обработчики, но с одинаковым типом.

    */
    bool registerEvent(std::function<IEventHandler> handler, std::string event_type);
    bool registerEvent(std::function<IEventHandler> handler, Timer timer);
    /**
        \brief сбрасывает все обработчики события, которые были зарегестрированы через данный
        экземпляр Connector
        \param[in] event_type строчка описывающа тип события, у которого надо сбросить обработчики
    */
    bool resetEvent(std::string &&event_type);
    bool resetEvent(const std::string &event_type);
private:
    ConnectorImpl* Pimpl();
private:
    ConnectorImpl *data;
    /**/
    //Требуется хранить те события, которые были зарегистрированы через данный коннектор
};

#endif //MAIN_PROJECT_CONNECTOR_H
