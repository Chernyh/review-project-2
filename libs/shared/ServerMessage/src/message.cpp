//
// Created by znujko on 24.11.2019.
//


#include "message.h"



std::string IMessage::getType() {

    return type;
}


IMessage::IMessage(std::string _type) {

    type = _type;
}

IMessage::IMessage() {

    type = "";
}

IMessage::~IMessage() {

    type = "";
}