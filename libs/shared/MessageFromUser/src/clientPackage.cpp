//
// Created by znujko on 28.11.2019.
//


#include "clientPackage.h"

CLIENT_PACKAGE::CLIENT_PACKAGE(Commands type, double commandTime, unsigned char _userId) : IMessage("SM") {

    typeOfMessage = type;
    messageTime = commandTime;
    userId = _userId;
}