//
// Created by znujko on 28.11.2019.
//

#ifndef MAIN_PROJECT_CLIENTPACKAGE_H
#define MAIN_PROJECT_CLIENTPACKAGE_H

#include "packageTypes.h"
#include "message.h"


class CLIENT_PACKAGE : public IMessage {
public :
    explicit CLIENT_PACKAGE(Commands type, double commandTime = 0, unsigned char _userId = 0);

    Commands typeOfMessage;
    double messageTime;
    unsigned char userId;
};


#endif //MAIN_PROJECT_CLIENTPACKAGE_H
