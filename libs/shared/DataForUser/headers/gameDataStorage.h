//
// Created by znujko on 06.11.2019.
//

#ifndef GLIPGAME_GAMEDATAHANDLER_H
#define GLIPGAME_GAMEDATAHANDLER_H


#include <string>
#include <queue>
#include <utility>
#include "data.h"
#include "packageTypes.h"
#include "UITypes.h"

class DefActionQueue {
public :
    virtual Commands popAction() = 0;

    virtual void addAction(Commands stringAdded) = 0;
};


class ActionQueue : public DefActionQueue {
public :

    ActionQueue();

    ~ActionQueue();

    Commands popAction() override;

    void addAction(Commands stringAdded) override;

    std::queue<Commands> actionQueue;
};

class SpriteQueue {
public :


    SpriteQueue();

    ~SpriteQueue();

    std::pair<std::pair<ActionsUI, GameSprite *>, std::pair<Sprite, Position>> popAction();

    GameSprite *popGameSprite();

    void addAction(ActionsUI actionType, GameSprite *&gSprite, const Sprite &sprite, const Position &position);

    void addGameSprite(GameSprite *&gSprite);

    bool spriteQueueisEmpty();

private :

    std::queue<std::pair<std::pair<ActionsUI, GameSprite *>, std::pair<Sprite, Position>>> spriteQueue;
    std::queue<GameSprite *> gameSpriteQueue;

};






#endif //GLIPGAME_GAMEDATAHANDLER_H
