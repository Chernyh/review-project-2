//
// Created by znujko on 07.11.2019.
//

#include "gameDataStorage.h"

Commands ActionQueue::popAction() {

    if(actionQueue.empty())
    {
        return Commands  :: ZERO ;
    }

    Commands result = actionQueue.front();
    actionQueue.pop();
    return result;

}

void ActionQueue::addAction(Commands stringAdded) {

    actionQueue.push(stringAdded);

}

ActionQueue::ActionQueue() {

    while(!actionQueue.empty())
        actionQueue.pop();

}

ActionQueue::~ActionQueue() {

    while(!actionQueue.empty())
        actionQueue.pop();
}


SpriteQueue::SpriteQueue() {

    while (!spriteQueue.empty())
        spriteQueue.pop();

}

SpriteQueue::~SpriteQueue() {

    while (!spriteQueue.empty())
        spriteQueue.pop();

}


std::pair<std::pair<ActionsUI, GameSprite *>, std::pair<Sprite, Position>> SpriteQueue::popAction() {

    std::pair<std::pair<ActionsUI, GameSprite *>, std::pair<Sprite, Position>> ret = spriteQueue.front();
    spriteQueue.pop();
    return ret;


}

bool SpriteQueue::spriteQueueisEmpty() {
    return spriteQueue.empty();
}

void
SpriteQueue::addAction(ActionsUI actionType, GameSprite *&gSprite, const Sprite &sprite, const Position &position) {

    spriteQueue.push(std::make_pair(std::make_pair(actionType, gSprite), std::make_pair(sprite, position)));

}

GameSprite *SpriteQueue::popGameSprite() {

    auto ret = gameSpriteQueue.front();
    gameSpriteQueue.pop();

    return ret;

}

void SpriteQueue::addGameSprite(GameSprite *&gSprite) {

    gameSpriteQueue.push(gSprite);

}



