//
// Created by znujko on 06.11.2019.
//

#ifndef GLIPGAME_DATA_H
#define GLIPGAME_DATA_H

#include <utility>
#include "gameSprite.h"

struct Sprite {
    int spriteID;
    int spriteFrame;
    int spriteAction;
    int maxFrame;

    Sprite();

    Sprite(int idInit, int frameInit, int actionInit, int maxedInit);

    Sprite(const Sprite &spriteInit);

    bool operator==(const Sprite &right) const;
};

struct Position {
    double x;
    double y;

    Position();

    Position(double xInit, double yInit);

    Position(const Position &positionInit);

    bool operator==(const Position &right) const;


};

class DefaultObject {
public:
    Sprite *sprite;

    DefaultObject();

    virtual ~DefaultObject();

    DefaultObject(int idInit, int frameInit, int actionInit, int maxedInit);

    DefaultObject(const DefaultObject &);

    explicit DefaultObject(const Sprite &spriteInit);


};

class Map : public DefaultObject {
public:
    Map();

    ~Map();

    Map(const Map &);

    bool operator==(const Map &right) const;

    explicit Map(int idInit, int frameInit = 0, int actionInit = 0, int maxedInit = 0);

    explicit Map(const Sprite &sprite1);
};

class GameObject : public DefaultObject {
public :
    Position *currentPosition;

    GameSprite *drawSprite;

    std::pair<double, double> currentSpeed;

    GameObject();

    ~GameObject();

    GameObject(Position positionInit, std::pair<double, double> speedInit, Sprite spriteInit);


    bool operator==(const GameObject &right) const;


};

class Bullet : public GameObject {
public :
    int damage;

    Bullet(Position positionInit, std::pair<double, double> speedInit, Sprite spriteInit,
           int damageInit);

    Bullet();

    ~Bullet();

    bool operator==(const Bullet &right) const;
};

class Weapon : public GameObject {
public:
    bool isPicked;
    int bulletCapacity;
    double fireSpeed;
    double timeForShot;

    Weapon(Position positionInit, std::pair<double, double> speedInit, Sprite spriteInit,
           bool isPickedInit, int bulletCapacityInit, double fireSpeedInit, double timeForShootInit);

    Weapon(const Weapon &weaponInit);

    Weapon();

    ~Weapon();

    bool operator==(const Weapon &right) const;

};

class Player : public GameObject {
public:
    int playerId;
    int playerHp;
    Weapon *playerWeapon;

    Player();

    Player(Position positionInit, std::pair<double, double> speedInit, Sprite spriteInit,
           int playerIdInit, int playerHpInit, Weapon *playerWeaponInit = nullptr);

    ~Player();

    bool operator==(const Player &right) const;

};

#endif //GLIPGAME_DATA_H
