//
// Created by znujko on 07.11.2019.
//


#include <tuple>
#include "data.h"

Sprite::Sprite(int idInit, int frameInit, int actionInit, int maxedInit) : spriteID(idInit), spriteFrame(frameInit),
                                                                           spriteAction(actionInit),
                                                                           maxFrame(maxedInit) {

}

Sprite::Sprite(const Sprite &spriteInit) : spriteID(spriteInit.spriteID), spriteFrame(spriteInit.spriteFrame),
                                           spriteAction(spriteInit.spriteAction), maxFrame(spriteInit.maxFrame) {

}

Sprite::Sprite() : spriteID(0), spriteFrame(0),
                   spriteAction(0), maxFrame(0) {

}

bool Sprite::operator==(const Sprite &right) const {
    return std::tie(spriteID, spriteFrame, spriteAction, maxFrame) ==
           std::tie(right.spriteID, right.spriteFrame, right.spriteAction, right.maxFrame);

}

Position::Position(double xInit, double yInit) : x(xInit), y(yInit) {


}

Position::Position(const Position &positionInit) : x(positionInit.x), y(positionInit.y) {


}

Position::Position() : x(0), y(0) {

}

bool Position::operator==(const Position &right) const {

    return std::tie(x, y) == std::tie(right.x, right.y);

}

DefaultObject::DefaultObject() : sprite(nullptr) {

}

DefaultObject::DefaultObject(int idInit, int frameInit, int actionInit, int maxedInit) : sprite(
        new Sprite(idInit, frameInit, actionInit, maxedInit)) {
}

DefaultObject::DefaultObject(const DefaultObject &defaultObjectInit) : sprite(new Sprite(*defaultObjectInit.sprite)) {


}

DefaultObject::DefaultObject(const Sprite &spriteInit) : sprite(new Sprite(spriteInit)) {


}

DefaultObject::~DefaultObject() {
    if (sprite)
        delete sprite;

}


Map::Map(int idInit, int frameInit, int actionInit, int maxedInit) : DefaultObject(idInit, frameInit, actionInit,
                                                                                   maxedInit) {

}

Map::Map(const Map &mapInit) : DefaultObject(*(mapInit.sprite)) {


}

Map::Map(const Sprite &spriteInit) : DefaultObject(
        spriteInit) {

}

Map::Map() : DefaultObject() {

}

Map::~Map() {

}

bool Map::operator==(const Map &right) const {
    return *(sprite) == *(right.sprite);
}


GameObject::GameObject() : DefaultObject(), drawSprite(nullptr), currentPosition(nullptr) {

}

GameObject::GameObject(Position positionInit, std::pair<double, double> speedInit, Sprite spriteInit) : DefaultObject(
        spriteInit), currentSpeed(std::make_pair(speedInit.first, speedInit.second)), currentPosition(), drawSprite() {

    currentPosition = new Position(positionInit);
    drawSprite = new GameSprite(spriteInit.spriteID);


}

bool GameObject::operator==(const GameObject &right) const {
    return std::tie(*sprite, *currentPosition, currentSpeed) ==
           std::tie(*right.sprite, *right.currentPosition, right.currentSpeed);

}

GameObject::~GameObject() {
    delete currentPosition;
    currentSpeed.first = 0;
    currentSpeed.second = 0;
    delete drawSprite;
}

Bullet::Bullet(Position positionInit, std::pair<double, double> speedInit, Sprite spriteInit, int damageInit)
        : GameObject(positionInit, speedInit, spriteInit) {
    damage = damageInit;
}

bool Bullet::operator==(const Bullet &right) const {

    return std::tie(damage, currentSpeed, currentPosition, sprite) ==
           std::tie(right.damage, right.currentSpeed, right.currentPosition, right.sprite);


}

Bullet::Bullet() : GameObject() {

}

Bullet::~Bullet() {
    damage = 0;
}

Weapon::Weapon(Position positionInit, std::pair<double, double> speedInit, Sprite spriteInit, bool isPickedInit,
               int bulletCapacityInit, double fireSpeedInit, double timeForShootInit) : GameObject(positionInit,
                                                                                                   speedInit,
                                                                                                   spriteInit) {

    isPicked = isPickedInit;
    bulletCapacity = bulletCapacityInit;
    fireSpeed = fireSpeedInit;
    timeForShot = timeForShootInit;

}

bool Weapon::operator==(const Weapon &right) const {

    return std::tie(isPicked, bulletCapacity, fireSpeed, timeForShot, currentSpeed,
                    *currentPosition, *sprite) ==
           std::tie(right.isPicked, right.bulletCapacity, right.fireSpeed, right.timeForShot, right.currentSpeed,
                    *right.currentPosition, *right.sprite);

}


Weapon::Weapon(const Weapon &weaponInit) : GameObject(*weaponInit.currentPosition, weaponInit.currentSpeed,
                                                      *weaponInit.sprite) {

    isPicked = weaponInit.isPicked;
    bulletCapacity = weaponInit.bulletCapacity;
    fireSpeed = weaponInit.fireSpeed;
    timeForShot = weaponInit.timeForShot;

}

Weapon::Weapon() : GameObject() {

    isPicked = false;
    bulletCapacity = 0;
    fireSpeed = 0;
    timeForShot = 0;

}

Weapon::~Weapon() {

    isPicked = false;
    bulletCapacity = 0;
    fireSpeed = 0;
    timeForShot = 0;

}

Player::Player(Position positionInit, std::pair<double, double> speedInit, Sprite spriteInit, int playerIdInit,
               int playerHpInit, Weapon *playerWeaponInit) : GameObject(positionInit, speedInit, spriteInit) {

    playerId = playerIdInit;
    playerHp = playerHpInit;
    playerWeapon = playerWeaponInit;

}

Player::Player() : GameObject() {

    playerId = -1;
    playerHp = 100;
    playerWeapon = nullptr;

}

Player::~Player() {

    playerId = -1;
    playerHp = -1;
    playerWeapon = nullptr;
}

bool Player::operator==(const Player &right) const {

    return std::tie(playerWeapon, playerHp, playerId,
                    currentSpeed,
                    *currentPosition,
                    *sprite) ==
           std::tie(right.playerWeapon, right.playerHp, right.playerId,
                    right.currentSpeed,
                    *right.currentPosition,
                    *right.sprite);


}

